--task1  (lesson8)
-- oracle: https://leetcode.com/problems/department-top-three-salaries/
SELECT d.Name as Department,
e.Name as Employee,
e.Salary as Salary
FROM Department d, Employee e
WHERE(
    SELECT COUNT(distinct Salary)
    FROM Employee
    WHERE Salary > e.Salary AND DepartmentId = d.Id
) < 3 AND e.DepartmentId = d.Id
ORDER BY d.Id, e.Salary DESC

--task2  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/17
SELECT fm.member_name, fm.status, SUM(p.amount*p.unit_price) as costs
FROM FamilyMembers as fm
JOIN Payments as p 
ON p.family_member = fm.member_id
WHERE YEAR(p.date) = 2005
GROUP BY fm.member_id, fm.member_name, fm.status

--task3  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/13
SELECT name
FROM Passenger 
GROUP BY name  
HAVING COUNT(*) > 1


--task4  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/38
select COUNT(first_name) as COUNT 
from Student
where first_name = 'Anna' 


--task5  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/35
SELECT COUNT(date) as Count
FROM Schedule
WHERE date = '2019-09-02'


--task6  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/38

--task7  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/32
select floor(avg((YEAR(CURRENT_DATE) - YEAR(birthday)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthday, '%m%d')))) as age
from FamilyMembers


--task8  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/27
select good_type_name, sum(unit_price) as costs
from 
(
select unit_price, good_type_name
from GoodTypes gt
join Payments p2
on gt.good_type_id= p2.amount
where p2.date like '2005%'
) a
group by good_type_name


--task9  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/37
select (YEAR(CURRENT_DATE) - YEAR(birthday)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthday, '%m%d')) as year
from Student
where birthday = (select max(birthday) from Student)


--task10  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/44
select max(max_year_1) as max_year
    from(select (YEAR(CURRENT_DATE) - YEAR(s.birthday)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(s.birthday, '%m%d')) as max_year_1
    from Student s
    join Student_in_class sc 
    on s.id=sc.student
    join Class c
    on c.id=sc.class
    where c.name LIKE '10%')a
    
--task11 (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/20
select fm.status,fm.member_name,sum(p.amount*p.unit_price)as costs
from FamilyMembers fm
JOIN Payments  p 
ON p.family_member = fm.member_id
join Goods g 
on p.good=g.good_id
join GoodTypes gt
on g.type=gt.good_type_id
where gt.good_type_name='entertainment'
GROUP BY fm.member_name, fm.status


--task12  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/55
delete from Company
    where id in 
    (select id
    from(select c.id, count(t.company) as count
    from Company c
    JOIN Trip t
    on c.id=t.Company
   group by t.company
    ORDER BY count LIMIT 3)a)


--task13  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/45
select classroom
from (select count(classroom) as COUNT, classroom from Schedule GROUP BY classroom ORDER BY COUNT DESC) a
where count > 4

--task14  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/43
SELECT last_name
from Schedule s1
JOIN Subject s2 
on s1.subject=s2.id
JOIN Teacher t 
on s1.teacher=t.id
where name = 'Physical Culture'
ORDER BY Teacher 

--task15  (lesson8)
-- https://sql-academy.org/ru/trainer/tasks/63
select CONCAT( last_name, '.', LEFT(first_name,1), '.', LEFT(middle_name,1), '.' ) AS name
from Student
ORDER BY name ASC 